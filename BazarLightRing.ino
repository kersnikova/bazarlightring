#include <Adafruit_NeoPixel.h>
#include<Wire.h>

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
const int pixelCnt = 16;
const int pixelPin = 2;
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(pixelCnt, pixelPin, NEO_GRB + NEO_KHZ800);

// MPU-6050 Sensor
const int MPU_addr = 0x68;  // I2C address
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

void setup(void) {
  Serial.begin(9600);
  Serial.println("Hello!");

  pixels.begin();
  pixels.show(); 

  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
}

long cnt = 0;
int X, Y;

int AcX0 = 0, AcY0 = 0;

void loop() {
  cnt++;

  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
  Serial.print("AcX = "); Serial.print(AcX);
  Serial.print(" | AcY = "); Serial.print(AcY);
  Serial.print(" | AcZ = "); Serial.print(AcZ);
  Serial.print(" | Tmp = "); Serial.print(Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet
  Serial.print(" | GyX = "); Serial.print(GyX);
  Serial.print(" | GyY = "); Serial.print(GyY);
  Serial.print(" | GyZ = "); Serial.println(GyZ);
  if (AcX0 == 0) AcX0 = AcX;
  if (AcY0 == 0) AcY0 = AcY;

  X = abs(AcX-AcX0) < 500 ? 0 : ((AcX > AcX0) ? 1 : -1);
  Y = abs(AcY-AcY0) < 500 ? 0 : ((AcY > AcY0) ? 1 : -1);

  for (int i = 0; i < pixelCnt; i++) {
    pixels.setPixelColor(i, 0, 0, 0);
    if (X == -1 && i < 4)             pixels.setPixelColor(i, 0, 0, 15); else
    if (Y == -1 && i < 8 && i >= 4)   pixels.setPixelColor(i, 0, 15, 0); else
    if (X == 1 && i < 12 && i >= 8)  pixels.setPixelColor(i, 15, 0, 15); else
    if (Y == 1 && i < 16 && i >= 12) pixels.setPixelColor(i, 15, 0, 0); else
      pixels.setPixelColor(i, 0, 0, 0);
  }

  pixels.show();
}
